dpf-plugins (1.7+ds-3) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Also link against system's RtAudio
  * Add patch to fix compilation with RtAudio6 (Closes: #1051558)
  * Bump standards version to 4.7.0
  * Apply 'wrap-and-sort -ast'

  [ Dennis Braun ]
  * Change B-D from pkg-config to pkgconf
  * Use default salsa ci config

 -- Dennis Braun <snd@debian.org>  Sat, 17 Aug 2024 14:35:06 +0200

dpf-plugins (1.7+ds-2) unstable; urgency=medium

  * Source-only upload

 -- Dennis Braun <snd@debian.org>  Sun, 27 Aug 2023 23:49:43 +0200

dpf-plugins (1.7+ds-1) unstable; urgency=medium

  [ Olivier Humbert ]
  * d/control: update Homepage link

  [ Dennis Braun ]
  * New upstream version 1.7+ds
  * Build-Depends: libgl1-mesa-dev => libgl-dev
  * Bump Standards-Version to 4.6.2
  * Provide the clap package of dpf-plugins
  * Provide the vst3 package of dpf-plugins
  * Add the new plugin packages to the Depends of the meta package
  * Refresh patches, drop armel patch applied by upstream
  * d/copyright: Refresh file entries, bump years

 -- Dennis Braun <snd@debian.org>  Thu, 27 Jul 2023 16:04:49 +0200

dpf-plugins (1.6+ds-2) unstable; urgency=medium

  * Fix FTBS on armel
  * Add libglm-dev, libxcursor-dev & libxrandr-de to B-Ds
  * Remove libjack-dev from B-Ds
    + https://github.com/DISTRHO/DPF-Plugins/issues/23#issuecomment-1360298123
  * Change my email address

 -- Dennis Braun <snd@debian.org>  Wed, 21 Dec 2022 20:00:58 +0100

dpf-plugins (1.6+ds-1) unstable; urgency=medium

  * New upstream version 1.6

  [ Dennis Braun ]
  * Introduce the dpf-source package, sources of the DISTRHO Plugin Framework
  * Build verbose, and try to fix CPPFLAGS missing
  * Refresh patchset, use rtmidi from system too
  * Prefer libjack-dev over libjack-jackd2-dev

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Fix passing of *FLAGS
  * Verbose builds
  * Replace dh_auto_install override with an execute_after
  * Allow for cross-compilation
  * Do not optimized binaries beyond our base architecture
  * Pass CPPFLAGS via CFLAGS/CXXFLAGS
  * Make sure to leave the sources in a pristine state
  * Make blhc ignore lines starting with "Compiling ..."

  [ Dennis Braun ]
  * d/not-installed: clap and vst3 format is so far not in use
  * d/copyright: Remove obsolete vestige entry
  * Use DejaVuSans from fonts-dejavu-core
  * Refresh lintian-overrides

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Update d/copyright
    + Exclude binary artifacts from the package import
    + Fix typo in upstream URL
    + Add 'licensecheck' target to d/rules
    + Generate d/copyright_hints
  * Automatically add repacksuffix when importing new upstream sources

 -- Dennis Braun <snd@debian.org>  Sun, 18 Dec 2022 21:56:25 +0100

dpf-plugins (1.5-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Use correct machine-readable copyright file URI.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

  [ Dennis Braun ]
  * New upstream version 1.5
    + Fix FTBFS with LTO (link time optimization) enabled (Closes: #1015389)
  * Use rtaudio system lib
  * Drop patch applied by upstream
  * Bump Standards-Version to 4.6.1
  * Update d/copyright entries and years
  * Update lintian-overrides
  * Add salsa ci config

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 11 Sep 2022 15:15:44 +0200

dpf-plugins (1.4-1) unstable; urgency=medium

  * Initial debian release (Closes: #953129)

 -- Dennis Braun <d_braun@kabelmail.de>  Fri, 05 Feb 2021 17:11:49 +0100
